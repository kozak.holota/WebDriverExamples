import os
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
chrome_browser.maximize_window()
chrome_browser.get('https://www.python.org')
about_item = chrome_browser.find_element_by_id("about")
png_screenshot = about_item.screenshot_as_png

if not "img" in os.listdir():
  os.mkdir("img")

with open(os.path.join("img", "about_menuitem.png"), "wb") as pp:
  pp.write(png_screenshot)

chrome_browser.quit()