import os
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
chrome_browser.maximize_window()
chrome_browser.get('https://www.python.org')
png_screenshot = chrome_browser.get_screenshot_as_png()

if not "img" in os.listdir():
    os.mkdir("img")

with open(os.path.join("img", "python_site.png"), "wb") as pp:
    pp.write(png_screenshot)
chrome_browser.quit()