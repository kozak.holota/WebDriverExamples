import os
from selenium.webdriver import Firefox
from webdriver_manager.firefox import GeckoDriverManager

firefox_browser = Firefox(executable_path=GeckoDriverManager().install())
firefox_browser.maximize_window()
firefox_browser.get('https://www.java.com')
if not "img" in os.listdir():
  os.mkdir("img")
firefox_browser.get_screenshot_as_file(os.path.join("img", "java_site.png"))
firefox_browser.quit()