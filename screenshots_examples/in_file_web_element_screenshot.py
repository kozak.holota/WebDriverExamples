import os
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
chrome_browser.maximize_window()
chrome_browser.get('https://www.python.org')
about_item = chrome_browser.find_element_by_id("about")

if not "img" in os.listdir():
  os.mkdir("img")

about_item.screenshot(os.path.join("img", "about_menuitem_1.png"))

chrome_browser.quit()