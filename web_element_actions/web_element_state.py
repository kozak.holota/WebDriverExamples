from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
chrome_browser.maximize_window()
chrome_browser.get('https://www.python.org')

example_ul = chrome_browser.find_element_by_id("launch-shell")

print(f"Attribute 'style': {example_ul.get_attribute('style')}")

print(f"Property 'id': {example_ul.get_property('id')}")

print(f"Tag name: {example_ul.tag_name}")

print(f"Element text: {example_ul.text}")

print(f"Element location: {example_ul.location}")

print(f"Element size: {example_ul.size}")
chrome_browser.quit()