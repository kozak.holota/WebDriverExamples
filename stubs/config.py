import random
import string


def get_random_string(size=10):
  return ''.join(random.choices([*string.ascii_letters, *string.digits], k=size))


class Config:
    browser = None
    username = get_random_string()
    password = get_random_string()
    email = f"{get_random_string()}@gmail.com"
