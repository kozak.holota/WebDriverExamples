from time import sleep

from selenium.webdriver import Remote, DesiredCapabilities

capabilities = DesiredCapabilities.CHROME

remote_firefox = Remote(command_executor='http://localhost:4444/wd/hub', desired_capabilities=capabilities)
remote_firefox.maximize_window()
remote_firefox.get("https://www.ebay.com")

sleep(5)

remote_firefox.quit()