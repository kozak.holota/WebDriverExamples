import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager
from stubs.config import Config


@pytest.fixture(scope='module', autouse=True)
def browser(request):
  Config.browser = Chrome(executable_path=ChromeDriverManager().install())
  Config.browser.maximize_window()
  Config.browser.get('https://www.python.org')

  request.addfinalizer(Config.browser.quit)