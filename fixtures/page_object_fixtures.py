from webbrowser import Chrome

import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

from helper.wrappers import get_random_string
from page_objects.bing_page_object import BingPageObject
from page_objects.python_site.main_page import MainPage


@pytest.fixture
def bing_page():
    chrome_browser = Chrome(executable_path=ChromeDriverManager(log_level="INFO").install())
    chrome_browser.maximize_window()

    yield BingPageObject(chrome_browser).open()

    chrome_browser.quit()


@pytest.fixture
def email():
    return f"{get_random_string()}@gmail.com"


@pytest.fixture
def username():
    return get_random_string()


@pytest.fixture
def password():
    return get_random_string()


@pytest.fixture
def main_page(request):
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.maximize_window()
    driver.get('https://www.python.org')

    main_page = MainPage(driver)

    def fin():
        driver.quit()

    request.addfinalizer(fin)

    return main_page