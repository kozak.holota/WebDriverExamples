from time import sleep

from selenium.webdriver import Chrome

import pytest as pytest
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver import Edge


@pytest.fixture
def browser():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.python.org/")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def pydocs_browser(request):
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()
    chrome_browser.get('https://docs.python.org')

    def fin():
        chrome_browser.quit()

    request.addfinalizer(fin)

    return chrome_browser


@pytest.fixture
def browser1():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://java.com/en/")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def ebay_browser():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.ebay.com/")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def joom_browser():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.joom.com/en")

    sleep(5)

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def google_browser(request):
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()
    chrome_browser.get('https://www.google.com')

    def fin():
        chrome_browser.quit()

    request.addfinalizer(fin)

    return chrome_browser


@pytest.fixture
def prompts_game():
    return "if(confirm('Do you want to proceed?')) {alert('Lets Proceed')} else {alert('Goodbye')}"


@pytest.fixture
def bing_browser():
    chrome_browser = Edge(executable_path=EdgeChromiumDriverManager(log_level="INFO").install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.bing.com/")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def bing_browser_implicit_wait():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.implicitly_wait(10)
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.bing.com/")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def viza_browser():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.travelvisapro.com/visa")

    yield chrome_browser

    chrome_browser.quit()


@pytest.fixture
def draken_sang_explicit_wait():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()

    chrome_browser.get("https://www.drakensang.com/en")

    yield WebDriverWait(chrome_browser, 30)

    chrome_browser.quit()
