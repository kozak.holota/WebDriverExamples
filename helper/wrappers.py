import random
import string

from stubs.config import Config


def click_on_section_link(txt):
  txt = txt.lower().replace(' ', '-')
  Config.browser.find_element_by_css_selector(f"[href='/{txt}/']").click()


def get_random_string(size=10):
  return ''.join(random.choices([*string.ascii_letters, *string.digits], k=size))