import time

from selenium.webdriver import Chrome
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = Chrome(executable_path=ChromeDriverManager().install())
driver.maximize_window()

driver.get("https://www.ebay.com")

input_field = driver.find_element(
    By.XPATH, "//input[@type='text' and @id='gh-ac']"
)

actions = action_chains.ActionChains(driver)

search_button = driver.find_element(By.CSS_SELECTOR, "#gh-btn")

input_field.send_keys("Astronomical Telescope")

####Perform actions###########

actions.click_and_hold(search_button)

actions.perform()

time.sleep(10)

driver.quit()