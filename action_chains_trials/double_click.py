import time
from selenium.webdriver.common.by import By
from selenium.webdriver import Firefox
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common import action_chains

driver = Firefox(executable_path=GeckoDriverManager().install())

driver.maximize_window()

driver.get("https://www.ebay.com")
input_field = driver.find_element(
    By.XPATH, "//input[@type='text' and @id='gh-ac']"
)

actions = action_chains.ActionChains(driver)

search_button = driver.find_element(By.CSS_SELECTOR, "#gh-btn")

input_field.send_keys("Astronomical Telescope")

######Actions######

actions.double_click(search_button).perform()

time.sleep(10)

driver.quit()