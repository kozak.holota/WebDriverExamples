import time

from selenium.webdriver import Firefox
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.keys import Keys
from webdriver_manager.firefox import GeckoDriverManager

driver = Firefox(executable_path=GeckoDriverManager().install())

driver.maximize_window()
driver.get("https://www.ebay.com")
actions = action_chains.ActionChains(driver)
advanced_link = driver.find_element_by_link_text("Advanced")

actions\
.move_to_element(advanced_link)\
.context_click().send_keys(Keys.ARROW_DOWN).perform()
time.sleep(5)

driver.quit()