import sys
import time

from selenium.webdriver import Firefox
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.keys import Keys
from webdriver_manager.firefox import GeckoDriverManager

driver = Firefox(executable_path=GeckoDriverManager().install())

driver.maximize_window()

driver.get("https://stackoverflow.com")

search_field = driver.find_element_by_css_selector("#search [type=text]")

actions = action_chains.ActionChains(driver)

control_key = None

if sys.platform == 'darwin':
    control_key = Keys.COMMAND
else:
    control_key = Keys.CONTROL

actions.key_down(control_key) \
    .send_keys("a") \
    .key_down(control_key) \
    .send_keys("c") \
    .click(search_field) \
    .key_down(control_key) \
    .send_keys("v")

time.sleep(3)

actions.perform()

time.sleep(10)

driver.quit()
