import pytest

from helper.wrappers import click_on_section_link
from stubs.config import Config
from web_page_locators.python_site_locators import PythonSiteWebElements


@pytest.mark.parametrize("section_name, expected_title", (
   ("Downloads", "Download Python | Python.org"),
   ("Community", "Our Community | Python.org"),
   ("Success Stories", "Our Success Stories | Python.org"),
   ("Events", "Our Events | Python.org")
  )
)
def test_about_page(section_name, expected_title):
  click_on_section_link(section_name)
  assert Config.browser.title == expected_title, "Incorrect Page title"


def test_register_membership():
    PythonSiteWebElements.MEMBERSHIP_BUTTON.get().click()
    PythonSiteWebElements.SIGNUP_BUTTON.get().click()
    PythonSiteWebElements.EMAIL_FIELD.get().send_keys(Config.email)
    PythonSiteWebElements.USERNAME_FIELD.get().send_keys(Config.username)
    PythonSiteWebElements.PASSWORD_FIELD.get().send_keys(Config.password)
    PythonSiteWebElements.PASSWORD_CONFIRMATION_FIELD.get().send_keys(Config.password)
    PythonSiteWebElements.SIGNUP_FORM_BUTTON.get().click()

    expected_verification_message = f"Confirmation e-mail sent to {Config.email}."
    actual_verification_message = PythonSiteWebElements.VERIFICATION_MESSAGE.get().text

    assert actual_verification_message == expected_verification_message, f"Unexpected verification message: {actual_verification_message}.\
  Should be: {expected_verification_message}"