import pytest
from selenium.webdriver import Chrome


class TestBrowserNavigation:
    @pytest.mark.back_forward_test
    def test_back_forward(self, browser: Chrome):
        python_url = "https://www.python.org/"
        java_url = "https://java.com/en/"

        python_title = "Welcome to Python.org"
        java_title = "Java | Oracle"

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

        browser.get(java_url)

        assert browser.current_url == java_url, f"Incorrect URL for the Java site: {browser.current_url}. Should be: {java_url}"
        # assert browser.title == java_title, f"Incorrect title: {browser.title}, should be: {java_title}"

        browser.back()

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

        browser.forward()

        assert browser.current_url == java_url, f"Incorrect URL for the Java site: {browser.current_url}. Should be: {java_url}"
        # assert browser.title == java_title, f"Incorrect title: {browser.title}, should be: {java_title}"

    @pytest.mark.refresh_test
    def test_refresh(self, browser: Chrome):
        python_url = "https://www.python.org/"
        python_title = "Welcome to Python.org"

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

        browser.refresh()

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

    @pytest.mark.two_windows_test
    def test_two_windows_mode(self, browser: Chrome, browser1: Chrome):
        python_url = "https://www.python.org/"
        java_url = "https://java.com/en/"

        python_title = "Welcome to Python.org"
        java_title = "Java | Oracle"

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

        assert browser1.current_url == java_url, f"Incorrect URL for the Java site: {browser.current_url}. Should be: {java_url}"
        # assert browser1.title == java_title, f"Incorrect title: {browser.title}, should be: {java_title}"

    @pytest.mark.two_tabs_mode_test
    def test_two_tabs(self, browser: Chrome):
        python_url = "https://www.python.org/"
        java_url = "https://java.com/en/"

        python_title = "Welcome to Python.org"
        java_title = "Java | Oracle"

        browser.execute_script(f"$(window.open('{java_url}'))")

        browser.switch_to.window(browser.window_handles[0])

        assert browser.current_url == python_url, f"Incorrect URL for the Python site: {browser.current_url}. Should be: {python_url}"
        assert browser.title == python_title, f"Incorrect title: {browser.title}, should be: {python_title}"

        browser.switch_to.window(browser.window_handles[1])

        assert browser.current_url == java_url, f"Incorrect URL for the Java site: {browser.current_url}. Should be: {java_url}"
        # assert browser.title == java_title, f"Incorrect title: {browser.title}, should be: {java_title}"

    @pytest.mark.alerts_test
    def test_alerts(self, browser: Chrome):
        alerts_script = "if(confirm('Do you want to continue?')) alert('Please, Continue...'); else alert('Good Bye');"

        browser.execute_async_script(alerts_script)

        browser.switch_to.alert.accept()

        continue_alert = browser.switch_to.alert

        assert continue_alert.text == 'Please, Continue...', "Incorrect alert appeared"

        continue_alert.accept()

        browser.execute_async_script(alerts_script)

        browser.switch_to.alert.dismiss()

        goodbye_alert = browser.switch_to.alert

        assert goodbye_alert.text == 'Good Bye', "Incorrect alert appeared"

        goodbye_alert.accept()
