def test_signup(main_page, email, username, password):
    signup_page = main_page.click_membership_button().click_signup_button()

    signup_page.enter_email(email)

    signup_page.enter_username(username)

    signup_page.enter_password(password)

    signup_page.confirm_password(password)

    signup_validation_page = signup_page.submit_registration()

    expected_verification_message = f"Confirmation e-mail sent to {email}."

    assert signup_validation_page.verification_message == expected_verification_message, \
        f"Unexpected verification message: {signup_validation_page.verification_message}.\
  Should be: {signup_validation_page.verification_message}"


def test_about_page(main_page):
    about_banner = main_page.go_to_about_page().about_banner

    assert about_banner.is_displayed(), "About Banner is not displayed"