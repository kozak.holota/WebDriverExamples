import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By


class TestWebElements:
    @pytest.mark.basic_web_element_search
    def test_basic_web_element_search(self, ebay_browser: Chrome):
        search_field = ebay_browser.find_element(By.XPATH, "//*[@name='_nkw']")
        search_button = ebay_browser.find_element(By.ID, "gh-btn")

        assert search_button.get_attribute("value") == "Search", "Incorrect label on the search button"

        search_field.send_keys("Bubochka")
        search_field.clear()

        search_field.send_keys("Barbie doll")

        search_button.click()

        found_dolls = ebay_browser.find_elements(By.CSS_SELECTOR, ".s-item")

        assert len(found_dolls), "No Barby found. Bad shop :("

        for item in found_dolls:
            assert item.is_displayed(), "Barbie item is not shown on the page"
            assert item.is_enabled(), "Found item is disabled. We can't buy it."

    @pytest.mark.normal_web_element_search
    def test_normal_web_element_search(self, ebay_browser: Chrome):
        search_field = ebay_browser.find_element_by_name('_nkw')
        search_button = ebay_browser.find_element_by_id("gh-btn")

        assert search_button.get_attribute("value") == "Search", "Incorrect label on the search button"

        search_field.send_keys("Bubochka")
        search_field.clear()

        search_field.send_keys("Barbie doll")

        search_button.click()

        found_dolls = ebay_browser.find_elements_by_class_name("s-item")

        assert len(found_dolls), "No Barby found. Bad shop :("

        for item in found_dolls:
            assert item.is_displayed(), "Barbie item is not shown on the page"
            assert item.is_enabled(), "Found item is disabled. We can't buy it."