import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestExplicitWait:
    @pytest.mark.basic_explicit_waits
    def test_basic_explicit_waits(self, draken_sang_explicit_wait: WebDriverWait):
        privacy_accept_button = draken_sang_explicit_wait.until(EC.element_to_be_clickable((By.XPATH, "//span[text()='AGREE']/ancestor::button")))
        privacy_accept_button.click()
        play_now_button = draken_sang_explicit_wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'Play now')]")))
        play_now_button.click()

        logo = draken_sang_explicit_wait.until(EC.visibility_of_element_located((By.ID, 'logo')))

        assert logo.is_enabled(), "Logo is not clickable"
