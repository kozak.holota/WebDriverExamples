import sys
from time import sleep

import pytest
from selenium.webdriver import ActionChains, Keys


class TestBing:
    @pytest.mark.simple_search
    def test_simple_search(self, bing_browser):
        actions = ActionChains(bing_browser)

        search_field = bing_browser.find_element_by_id("sb_form_q")
        search_button = bing_browser.find_element_by_id("sb_form_go")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").click(
            on_element=search_button).pause(5).perform()

        selenium_link = bing_browser.find_element_by_css_selector("a[title^=Selenium]")

        assert selenium_link.is_enabled(), "Selenium logo is not enabled"
        assert selenium_link.is_displayed(), "Selenium logo is not displayed"

    @pytest.mark.simple_search_by_enter
    def test_simple_search_by_enter(self, bing_browser):
        actions = ActionChains(bing_browser)

        search_field = bing_browser.find_element_by_id("sb_form_q")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").key_down(
            Keys.ENTER).key_up(Keys.ENTER).pause(5).perform()

        selenium_link = bing_browser.find_element_by_css_selector("a[title^=Selenium]")

        assert selenium_link.is_enabled(), "Selenium logo is not enabled"
        assert selenium_link.is_displayed(), "Selenium logo is not displayed"

    @pytest.mark.copy_paste_test
    def test_copy_paste(self, bing_browser):
        actions = ActionChains(bing_browser)

        search_field = bing_browser.find_element_by_id("sb_form_q")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").key_down(
            Keys.ENTER).key_up(Keys.ENTER).pause(5).perform()

        search_field = bing_browser.find_element_by_id("sb_form_q")

        command_key = Keys.COMMAND if sys.platform == "darwin" else Keys.CONTROL

        actions.key_down(command_key) \
            .send_keys('a') \
            .key_up(command_key) \
            .key_down(command_key) \
            .send_keys('c') \
            .key_up(command_key) \
            .pause(5) \
            .click(on_element=search_field) \
            .key_down(command_key) \
            .send_keys('v') \
            .perform()

        sleep(3)

    @pytest.mark.drag_and_drop
    def test_drag_and_drop(self, bing_browser):
        actions = ActionChains(bing_browser)

        search_field = bing_browser.find_element_by_id("sb_form_q")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").key_down(
            Keys.ENTER).key_up(Keys.ENTER).perform()

        sleep(5)

        selenium_link = bing_browser.find_element_by_css_selector("a[title^=Selenium]")

        actions.drag_and_drop_by_offset(source=selenium_link, xoffset=100, yoffset=20).drag_and_drop_by_offset(
            source=selenium_link, xoffset=0, yoffset=80).perform()

        sleep(3)

    @pytest.mark.drag_and_drop_to_element
    def test_drag_and_drop_to_element(self, bing_browser):
        actions = ActionChains(bing_browser)

        search_field = bing_browser.find_element_by_id("sb_form_q")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").key_down(
            Keys.ENTER).key_up(Keys.ENTER).perform()

        sleep(5)

        selenium_link = bing_browser.find_element_by_css_selector("a[title^=Selenium]")

        image_search_button = bing_browser.find_element_by_id("sb_sbi")

        image_search_button.click()

        image_search_box = bing_browser.find_element_by_class_name('dpnmpn')

        actions.drag_and_drop(selenium_link, image_search_box).perform()

        sleep(5)
