from time import sleep


class TestModalDialogs:
    def test_signin_dialog(self, joom_browser):
        search_button = joom_browser.find_element_by_css_selector(("button[type=submit]"))
        joom_browser.find_element_by_css_selector("a[href$=entrance]").click()

        sleep(5)

        joom_browser.find_element_by_css_selector("div[class^=sidebar]>div[class^=inner] [class^=close]").click()

        assert search_button.is_enabled(), "Search button is enabled when dialog is not active but shouldn't"

    def test_prompts(self, google_browser, prompts_game):
        accepted_text = "Lets Proceed"
        dismissed_text = "Goodbye"

        google_browser.execute_async_script(prompts_game)
        sleep(3)

        google_browser.switch_to.alert.accept()
        sleep(3)

        result_text = google_browser.switch_to.alert.text

        google_browser.switch_to.alert.accept()

        assert result_text == accepted_text, "Incorrect text if accepting the alert"

        google_browser.execute_async_script(prompts_game)
        sleep(3)

        google_browser.switch_to.alert.dismiss()
        sleep(3)

        result_text = google_browser.switch_to.alert.text

        google_browser.switch_to.alert.accept()

        assert result_text == dismissed_text, "Incorrect text if dismissing the alert"