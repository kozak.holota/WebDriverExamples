import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


class TestImplicitWait:
    @pytest.mark.drag_and_drop_to_element_implicit_wait
    def test_drag_and_drop_to_element_implicit_wait(self, bing_browser_implicit_wait):
        actions = ActionChains(bing_browser_implicit_wait)

        search_field = bing_browser_implicit_wait.find_element_by_id("sb_form_q")

        actions.click(on_element=search_field).send_keys_to_element(search_field, "python + selenium").key_down(
            Keys.ENTER).key_up(Keys.ENTER).perform()

        selenium_link = bing_browser_implicit_wait.find_element_by_css_selector("a[title^=Selenium]")

        image_search_button = bing_browser_implicit_wait.find_element_by_id("sb_sbi")

        image_search_button.click()

        image_search_box = bing_browser_implicit_wait.find_element_by_class_name('dpnmpn')

        actions.drag_and_drop(selenium_link, image_search_box)