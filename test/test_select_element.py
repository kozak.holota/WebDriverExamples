from time import sleep

import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select


class TestSelectElement:
    @pytest.mark.single_selection
    def test_single_selection(self, viza_browser):
        nationality_selector = viza_browser.find_element_by_id("nationality")
        nationality_menu = Select(nationality_selector)
        nationality_menu.select_by_value("13")
        sleep(3)
        nationality_menu.select_by_index(1)
        sleep(3)
        nationality_menu.select_by_visible_text("Costa Rica")
        sleep(3)

        assert nationality_menu.first_selected_option.text == "Costa Rica", "Incorrect nationality selected"

    @pytest.mark.python_doc_selection
    def test_python_version_doc_change(self, pydocs_browser):
        sleep(5)

        python_version_menu = Select(pydocs_browser.find_element_by_id("language_select"))
        python_version_menu.select_by_value("fr")

        sleep(5)

        doc_header = pydocs_browser.find_element_by_css_selector("div.body>h1")
        expected_doc_header = "Documentation Python 3.10.4"

        assert doc_header.text == expected_doc_header, f"Incorrect doc header: {doc_header.text}. Should be: {expected_doc_header}"
