import pytest

from page_objects.bing_page_object import BingPageObject


class TestPageObjects:
    @pytest.mark.explicit_wait_agains_stale_element_exception
    def test_explicit_wait_agains_stale_element_exception(self, bing_page: BingPageObject):
        bing_page.input_search_statement("python + selenium")

        bing_page.start_search_by_enter_key()

        bing_page.clear_search_field()

        bing_page.select_all_on_the_page_and_copy_to_clipboard()

        bing_page.insert_into_search_field_from_the_clipboard()

        assert len(bing_page.text_to_search), "No text from found results is inserted into the search field"