from enum import Enum

from selenium.webdriver.common.by import By

from stubs.config import Config


class PythonSiteWebElements(Enum):
    MEMBERSHIP_BUTTON = (By.CSS_SELECTOR, "a.button[href$='membership/']")
    SIGNUP_BUTTON = (By.CSS_SELECTOR, "a.button[href$='signup/']")
    EMAIL_FIELD = (By.ID, "id_email")
    USERNAME_FIELD = (By.ID, "id_username")
    PASSWORD_FIELD = (By.ID, "id_password1")
    PASSWORD_CONFIRMATION_FIELD = (By.ID, "id_password2")
    SIGNUP_FORM_BUTTON = (By.XPATH, "//button[contains(text(), 'Sign Up')]")
    VERIFICATION_MESSAGE = (By.CSS_SELECTOR, "div.user-feedback>p[role=general]")

    def __init__(self, location_strategy, locator):
        self.location_strategy = location_strategy
        self.locator = locator

    def get(self):
        return Config.browser.find_element(self.location_strategy, self.locator)