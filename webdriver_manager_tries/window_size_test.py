from time import sleep
from selenium.webdriver import Firefox
from webdriver_manager.firefox import GeckoDriverManager

firefox_browser = Firefox(executable_path=GeckoDriverManager().install())
firefox_browser.fullscreen_window()
sleep(5)
firefox_browser.minimize_window()
sleep(5)
firefox_browser.maximize_window()
sleep(5)
firefox_browser.set_window_size(500, 200)
sleep(5)
firefox_browser.quit()