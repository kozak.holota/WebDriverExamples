from time import sleep

from selenium.webdriver import Firefox, DesiredCapabilities
from webdriver_manager.firefox import GeckoDriverManager

capabilities = DesiredCapabilities.FIREFOX
capabilities['acceptInsecureCerts'] = False

print(capabilities)

firefox_browser = Firefox(executable_path=GeckoDriverManager().install(), desired_capabilities=capabilities)
firefox_browser.maximize_window()
firefox_browser.get("https://www.ebay.com")
sleep(5)
firefox_browser.quit()