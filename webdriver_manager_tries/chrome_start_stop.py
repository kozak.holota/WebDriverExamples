import os
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

chrome_options = Options()

# Denying browser addons

chrome_options.add_argument("--disable-extensions")

#Setting Download location to tghe current directory
chrome_options.add_argument(f"download.default_directory={os.path.abspath(os.curdir)}")

chrome_browser = Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
chrome_browser.maximize_window()

chrome_browser.get("https://www.ebay.com")

sleep(5)

print(chrome_browser.current_url)
print(chrome_browser.title)

chrome_browser.quit()