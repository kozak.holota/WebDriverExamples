from time import sleep

from selenium.webdriver import Edge
from webdriver_manager.microsoft import EdgeChromiumDriverManager

edge_browser = Edge(executable_path=EdgeChromiumDriverManager(log_level="INFO").install())
edge_browser.maximize_window()

edge_browser.get("https://www.ebay.com")

sleep(5)

edge_browser.quit()